package com.daxamayac.reactor.handler;

import com.daxamayac.reactor.model.Enrollment;
import com.daxamayac.reactor.service.IEnrollmentService;
import com.daxamayac.reactor.utils.SortUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class EnrollmentHandler {

    private final IEnrollmentService service;

    public EnrollmentHandler(IEnrollmentService service) {
        this.service = service;
    }

    public Mono<ServerResponse> findAll(ServerRequest req) {
        List<String> stringSorts = req.queryParams().get("sort");
        Sort sort = SortUtils.stringSortsToSort(stringSorts);
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(sort), Enrollment.class);
    }

    public Mono<ServerResponse> findById(ServerRequest req) {

        String id = req.pathVariable("id");

        return service.findById(id)
                .flatMap(p -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> findByStudentId(ServerRequest req) {

        String id = req.pathVariable("id");

        return service.findByStudentId(id)
                .flatMap(p -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest req) {
        Mono<Enrollment> monoEnrollment = req.bodyToMono(Enrollment.class);
        //TODO assert exists student and course

        return monoEnrollment
                .flatMap(service::create)
                .flatMap(p -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                );
    }

    public Mono<ServerResponse> delete(ServerRequest req) {
        String id = req.pathVariable("id");
        return service.delete(id)
                .then(ServerResponse.noContent().build());
    }

    public Mono<ServerResponse> update(ServerRequest req) {
        String id = req.pathVariable("id");
        Mono<Enrollment> monoEnrollment = req.bodyToMono(Enrollment.class);
        Mono<Enrollment> monoBD = service.findById(id);

        return monoBD
                .zipWith(monoEnrollment, (bd, enrollment) -> {
                    BeanUtils.copyProperties(enrollment, bd, "id");
                    return bd;
                })
                .flatMap(service::update)
                .flatMap(p -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

}
