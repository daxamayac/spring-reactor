package com.daxamayac.reactor.handler;

import com.daxamayac.reactor.model.Student;
import com.daxamayac.reactor.service.IStudentService;
import com.daxamayac.reactor.utils.SortUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class StudentHandler {

    private final IStudentService service;

    public StudentHandler(IStudentService service) {
        this.service = service;
    }


    public Mono<ServerResponse> findAll(ServerRequest req) {
        List<String> stringSorts = req.queryParams().get("sort");
        Sort sort = SortUtils.stringSortsToSort(stringSorts);

        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(service.findAll(sort), Student.class);
    }

    public Mono<ServerResponse> findById(ServerRequest req) {

        String id = req.pathVariable("id");

        return service.findById(id).flatMap(p -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(fromValue(p))).switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest req) {
        Mono<Student> monoStudent = req.bodyToMono(Student.class);
        return monoStudent.flatMap(service::create).flatMap(p -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(p.getId()))).contentType(MediaType.APPLICATION_JSON).body(fromValue(p)));
    }

    public Mono<ServerResponse> delete(ServerRequest req) {
        String id = req.pathVariable("id");

        return service.delete(id).then(ServerResponse.noContent().build());
    }

    public Mono<ServerResponse> update(ServerRequest req) {
        String id = req.pathVariable("id");
        Mono<Student> monoStudent = req.bodyToMono(Student.class);
        Mono<Student> monoBD = service.findById(id);

        return monoBD.zipWith(monoStudent, (bd, student) -> {
            BeanUtils.copyProperties(student, bd, "id");
            return bd;
        }).flatMap(service::update).flatMap(p -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(fromValue(p))).switchIfEmpty(ServerResponse.notFound().build());
    }

}
