package com.daxamayac.reactor.handler;

import com.daxamayac.reactor.security.AuthRequest;
import com.daxamayac.reactor.security.AuthResponse;
import com.daxamayac.reactor.security.JWTUtil;
import com.daxamayac.reactor.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Date;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class AuthHandler {

    private final IUserService service;
    private final JWTUtil jwtUtil;

    public AuthHandler(IUserService service, JWTUtil jwtUtil) {
        this.service = service;
        this.jwtUtil = jwtUtil;
    }

    public Mono<ServerResponse> signIn(ServerRequest req) {

        Mono<AuthRequest> monoAuthRequest = req.bodyToMono(AuthRequest.class);

        return monoAuthRequest.flatMap(authRequest -> service.findByUsername(authRequest.getUsername())
                .flatMap(customUserDetails -> {
                    if (BCrypt.checkpw(authRequest.getPassword(), customUserDetails.getPassword())) {
                        String token = jwtUtil.generateToken(customUserDetails);
                        Date expiration = jwtUtil.getExpirationDateFromToken(token);

                        return ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(fromValue(new AuthResponse(token, expiration)));
                    }
                    return ServerResponse.status(HttpStatus.UNAUTHORIZED).build();
                })
        ).switchIfEmpty(ServerResponse.status(HttpStatus.UNAUTHORIZED).build());
    }
}
