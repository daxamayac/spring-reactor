package com.daxamayac.reactor.utils;

import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

public class SortUtils {
    public static Sort stringSortsToSort(List<String> sorts) {

        if (sorts!=null && !sorts.isEmpty()) {

            List<Sort.Order> orders = new ArrayList<>();
            for (String stringSort : sorts) {
                stringSort = stringSort.contains(",") ? stringSort : stringSort.concat(",asc");
                String[] _sort = stringSort.split(",");
                Sort.Direction direction = _sort[1].equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;
                orders.add(new Sort.Order(direction, _sort[0]));
            }
            return Sort.by(orders);
        }

        return Sort.unsorted();
    }
}
