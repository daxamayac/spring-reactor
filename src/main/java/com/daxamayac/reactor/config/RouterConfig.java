package com.daxamayac.reactor.config;

import com.daxamayac.reactor.handler.AuthHandler;
import com.daxamayac.reactor.handler.CourseHandler;
import com.daxamayac.reactor.handler.EnrollmentHandler;
import com.daxamayac.reactor.handler.StudentHandler;
import com.daxamayac.reactor.model.Course;
import com.daxamayac.reactor.model.Enrollment;
import com.daxamayac.reactor.model.Student;
import com.daxamayac.reactor.security.AuthRequest;
import com.daxamayac.reactor.security.AuthResponse;
import io.netty.handler.logging.LoggingHandler;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springdoc.core.fn.builders.securityrequirement.Builder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springdoc.core.fn.builders.apiresponse.Builder.responseBuilder;
import static org.springdoc.core.fn.builders.parameter.Builder.parameterBuilder;
import static org.springdoc.core.fn.builders.requestbody.Builder.requestBodyBuilder;
import static org.springdoc.webflux.core.fn.SpringdocRouteBuilder.route;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class RouterConfig {

    private static final String TAG_STUDENTS = "Students";
    private static final String TAG_COURSES = "Courses";
    private static final String TAG_ENROLLMENTS = "Enrollments";

    private static final String TAG_AUTH = "Auth";

    @Bean
    public RouterFunction<ServerResponse> routesAuth(AuthHandler handler) {
        return route().POST("/v2/sign-in", accept(APPLICATION_JSON), handler::signIn, ops -> ops
                .tag(TAG_AUTH)
                .operationId("signIn")
                .summary("Sign in")
                .requestBody(requestBodyBuilder().implementation(AuthRequest.class).required(true))
                .response(responseBuilder().implementation(AuthResponse.class))).build();
    }
    @Bean
    public RouterFunction<ServerResponse> routesStudent(StudentHandler handler) {
        return route().GET("/v2/students", accept(APPLICATION_JSON), handler::findAll, ops -> ops
                        .tag(TAG_STUDENTS)
                        .operationId("getStudents")
                        .summary("Get all students")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .parameter(parameterBuilder().implementationArray(String.class).name("sort").example("name,desc"))
                        .response(responseBuilder().implementation(Student.class))).build()
                .and(route().GET("/v2/students/{id}", accept(APPLICATION_JSON), handler::findById, ops -> ops
                        .tag(TAG_STUDENTS)
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .operationId("getStudentById")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().POST("/v2/students", accept(APPLICATION_JSON), handler::create, ops -> ops
                        .tag(TAG_STUDENTS)
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .operationId("postStudents")
                        .summary("Create student")
                        .requestBody(requestBodyBuilder().implementation(Student.class).required(true))
                        .response(responseBuilder().responseCode("200").implementation(Student.class))).build())
                .and(route().DELETE("/v2/students/{id}", accept(APPLICATION_JSON), handler::delete, ops -> ops
                        .tag(TAG_STUDENTS)
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .operationId("deleteStudentById")
                        .summary("Delete student by Id")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().PUT("/v2/students/{id}", accept(APPLICATION_JSON), handler::update, ops -> ops
                        .tag(TAG_STUDENTS)
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .operationId("putStudent")
                        .summary("Update student")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))
                        .requestBody(requestBodyBuilder().implementation(Student.class).required(true))).build());
    }

    @Bean
    public RouterFunction<ServerResponse> routesCourse(CourseHandler handler) {
        return route().GET("/v2/courses", accept(APPLICATION_JSON), handler::findAll, ops -> ops
                        .tag(TAG_COURSES)
                        .operationId("getCourses")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Get all courses")
                        .response(responseBuilder().implementation(Course.class))).build()
                .and(route().GET("/v2/courses/{id}", accept(APPLICATION_JSON), handler::findById, ops -> ops
                        .tag(TAG_COURSES)
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .operationId("getCoursesById")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().POST("/v2/courses", accept(APPLICATION_JSON), handler::create, ops -> ops
                        .tag(TAG_COURSES)
                        .operationId("postCourses")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Create course")
                        .requestBody(requestBodyBuilder().implementation(Course.class).required(true))
                        .response(responseBuilder().responseCode("200").implementation(Course.class))).build())
                .and(route().DELETE("/v2/courses/{id}", accept(APPLICATION_JSON), handler::delete, ops -> ops
                        .tag(TAG_COURSES)
                        .operationId("deleteCoursesById")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Delete course by Id")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().PUT("/v2/courses/{id}", accept(APPLICATION_JSON), handler::update, ops -> ops
                        .tag(TAG_COURSES)
                        .operationId("putCourses")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Update course")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))
                        .requestBody(requestBodyBuilder().implementation(Course.class).required(true))).build());
    }

    @Bean
    public RouterFunction<ServerResponse> routesEnrollment(EnrollmentHandler handler) {
        return route().GET("/v2/enrollments", accept(APPLICATION_JSON), handler::findAll, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("getEnrollments")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Get all enrollments")
                        .response(responseBuilder().implementation(Enrollment.class))).build()
                .and(route().GET("/v2/enrollments/{id}", accept(APPLICATION_JSON), handler::findById, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("getEnrollmentById")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().GET("/v2/enrollments/students/{id}", accept(APPLICATION_JSON), handler::findByStudentId, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("getEnrollmentByStudentId")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().POST("/v2/enrollments", accept(APPLICATION_JSON), handler::create, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("postEnrollments")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Create enrollment")
                        .requestBody(requestBodyBuilder().implementation(Enrollment.class).required(true))
                        .response(responseBuilder().responseCode("200").implementation(Enrollment.class))).build())
                .and(route().DELETE("/v2/enrollments/{id}", accept(APPLICATION_JSON), handler::delete, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("deleteEnrollmentsById")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Delete enrollment by Id")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))).build())
                .and(route().PUT("/v2/enrollments/{id}", accept(APPLICATION_JSON), handler::update, ops -> ops
                        .tag(TAG_ENROLLMENTS)
                        .operationId("putEnrollments")
                        .security(Builder.securityRequirementBuilder().name("bearerAuth"))
                        .summary("Update enrollment")
                        .parameter(parameterBuilder().in(ParameterIn.PATH).name("id"))
                        .requestBody(requestBodyBuilder().implementation(Enrollment.class).required(true))).build());
    }

}
