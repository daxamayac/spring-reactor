package com.daxamayac.reactor.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "enrollments")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Enrollment {
    @Id
    private String id;
    private LocalDateTime enrollmentDate;
    private Student student;
    private List<Course> courses;
    private Boolean status;
}
