package com.daxamayac.reactor.repo;

import com.daxamayac.reactor.model.Enrollment;
import reactor.core.publisher.Mono;

public interface IEnrollmentRepo extends IGenericRepo<Enrollment,String> {
    Mono<Enrollment> findByStudentId(String studentId);
}
