package com.daxamayac.reactor.repo;

import com.daxamayac.reactor.model.Role;

public interface IRoleRepo extends IGenericRepo<Role, String> {

}
