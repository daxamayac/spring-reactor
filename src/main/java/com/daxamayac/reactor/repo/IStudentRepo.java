package com.daxamayac.reactor.repo;

import com.daxamayac.reactor.model.Student;

public interface IStudentRepo extends IGenericRepo<Student,String> {
}
