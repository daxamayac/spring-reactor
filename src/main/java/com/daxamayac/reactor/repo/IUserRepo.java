package com.daxamayac.reactor.repo;

import com.daxamayac.reactor.model.User;
import reactor.core.publisher.Mono;

public interface IUserRepo extends IGenericRepo<User, String>{
	Mono<User> findOneByUsername(String username);
}
