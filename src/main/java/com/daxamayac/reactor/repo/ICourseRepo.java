package com.daxamayac.reactor.repo;

import com.daxamayac.reactor.model.Course;
import reactor.core.publisher.Flux;

public interface ICourseRepo extends IGenericRepo<Course, String> {
    Flux<Course> findByStatus(boolean status);
}
