package com.daxamayac.reactor.service;

import com.daxamayac.reactor.model.Enrollment;
import reactor.core.publisher.Mono;

public interface IEnrollmentService extends ICRUD<Enrollment,String>{

    Mono<Enrollment> findByStudentId(String studentId);
}
