package com.daxamayac.reactor.service;

import org.springframework.data.domain.Sort;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICRUD<T, ID> {
    Mono<T> create(T t);

    Mono<T> update(T t);

    Flux<T> findAll(Sort sort);

    Mono<T> findById(ID id);

    Mono<Void> delete(ID id);

}
