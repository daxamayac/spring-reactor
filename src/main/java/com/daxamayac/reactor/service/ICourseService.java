package com.daxamayac.reactor.service;

import com.daxamayac.reactor.model.Course;
import reactor.core.publisher.Flux;

public interface ICourseService extends ICRUD<Course,String>{
    Flux<Course> findByStatus(boolean status);
}
