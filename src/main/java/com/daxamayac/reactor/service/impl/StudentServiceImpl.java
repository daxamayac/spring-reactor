package com.daxamayac.reactor.service.impl;

import com.daxamayac.reactor.model.Student;
import com.daxamayac.reactor.repo.IGenericRepo;
import com.daxamayac.reactor.repo.IStudentRepo;
import com.daxamayac.reactor.service.IStudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends CRUDImpl<Student,String> implements IStudentService {

    private final IStudentRepo repo;

    public StudentServiceImpl(IStudentRepo repo) {
        this.repo = repo;
    }

    @Override
    protected IGenericRepo<Student, String> getRepo() {
        return this.repo;
    }
}
