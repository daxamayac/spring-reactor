package com.daxamayac.reactor.service.impl;

import com.daxamayac.reactor.model.Course;
import com.daxamayac.reactor.repo.ICourseRepo;
import com.daxamayac.reactor.repo.IGenericRepo;
import com.daxamayac.reactor.service.ICourseService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class CourseServiceImpl extends CRUDImpl<Course,String> implements ICourseService {

    private final ICourseRepo repo;

    public CourseServiceImpl(ICourseRepo repo) {
        this.repo = repo;
    }

    @Override
    protected IGenericRepo<Course, String> getRepo() {
        return this.repo;
    }

    @Override
    public Flux<Course> findByStatus(boolean status) {
        return this.repo.findByStatus(status);
    }
}
