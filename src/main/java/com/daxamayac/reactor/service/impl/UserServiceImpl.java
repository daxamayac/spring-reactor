package com.daxamayac.reactor.service.impl;

import com.daxamayac.reactor.model.User;
import com.daxamayac.reactor.repo.IGenericRepo;
import com.daxamayac.reactor.repo.IRoleRepo;
import com.daxamayac.reactor.repo.IUserRepo;
import com.daxamayac.reactor.security.CustomUserDetails;
import com.daxamayac.reactor.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends CRUDImpl<User, String> implements IUserService {

    private final IUserRepo repo;

    private final IRoleRepo rolRepo;
    private BCryptPasswordEncoder bcrypt;

    @Override
    protected IGenericRepo<User, String> getRepo() {
        return repo;
    }

    @Override
    public Mono<CustomUserDetails> findByUsername(String username) {
        Mono<User> monoUsuario = repo.findOneByUsername(username);

        List<String> roles = new ArrayList<String>();

        return monoUsuario.flatMap(u -> {
                    return Flux.fromIterable(u.getRoles())
                            .flatMap(rol -> {
                                return rolRepo.findById(rol.getId())
                                        .map(r -> {
                                            roles.add(r.getName());
                                            return r;
                                        });
                            }).collectList().flatMap(list -> {
                                u.setRoles(list);
                                return Mono.just(u);
                            });
                })
                .flatMap(u -> {
                    return Mono.just(new CustomUserDetails(u.getUsername(), u.getPassword(), u.getStatus(), roles));
                });
    }

}
