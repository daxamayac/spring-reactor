package com.daxamayac.reactor.service.impl;

import com.daxamayac.reactor.model.Enrollment;
import com.daxamayac.reactor.repo.IEnrollmentRepo;
import com.daxamayac.reactor.repo.IGenericRepo;
import com.daxamayac.reactor.service.IEnrollmentService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class EnrollmentServiceImpl extends CRUDImpl<Enrollment,String> implements IEnrollmentService {

    private final IEnrollmentRepo repo;

    public EnrollmentServiceImpl(IEnrollmentRepo repo) {
        this.repo = repo;
    }

    @Override
    protected IGenericRepo<Enrollment, String> getRepo() {
        return this.repo;
    }

    @Override
    public Mono<Enrollment> findByStudentId(String studentId) {
        return this.repo.findByStudentId(studentId);
    }
}
