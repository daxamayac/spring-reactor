package com.daxamayac.reactor.service;

import com.daxamayac.reactor.model.User;
import com.daxamayac.reactor.security.CustomUserDetails;
import reactor.core.publisher.Mono;

public interface IUserService extends ICRUD<User, String>{
	Mono<CustomUserDetails> findByUsername(String username);
}
